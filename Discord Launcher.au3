#RequireAdmin
#NoTrayIcon
#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_Icon=icon.ico
#AutoIt3Wrapper_Outfile=Discord Launcher.exe
#AutoIt3Wrapper_Compression=4
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****

;==========================================GUI CREATION AND SETTINGS=========================================
#include <GUIConstantsEx.au3>
;!Highly recommended for improved overall performance and responsiveness of the GUI effects etc.! (after compiling):
;Required if you want High DPI scaling enabled. (Also requries _Metro_EnableHighDPIScaling())
#include "MetroGUI-UDF\MetroGUI_UDF.au3"
;Set Theme
_SetTheme("DarkPurple") ;See MetroThemes.au3 for selectable themes or to add more
;Enable high DPI support: Detects the users DPI settings and resizes GUI and all controls to look perfectly sharp.
_Metro_EnableHighDPIScaling() ; Note: Requries "#AutoIt3Wrapper_Res_HiDpi=y" for compiling. To see visible changes without compiling, you have to disable dpi scaling in compatibility settings of Autoit3.exe
;=============================================================================================================

;INCLUDES - These are required libraries for the script to function.

#include <MsgBoxConstants.au3>
#include <FileConstants.au3>
#include <ButtonConstants.au3>
#include <EditConstants.au3>
#include <WindowsConstants.au3>
#include <Constants.au3>
#include <Array.au3>
#include <File.au3>
#include <StaticConstants.au3>

;The script here looks for the config.ini file. If it doesn't exist it creates it and stores a startup variable to let
;you know it's been run at least once, and creates a shortcut on the desktop for ease of use.
If FileExists("config.ini") Then
	MainMenu()
Else
	FileOpen(@ScriptDir & "\config.ini", 1)
	IniWrite(@ScriptDir & "\config.ini", "Startup", "Startup", "0")
	Local Const $sFilePath = @DesktopDir & "\Discord Launcher.lnk"
	FileCreateShortcut(@ScriptDir & "\Discord Launcher.exe", $sFilePath, @ScriptDir, "", "Discord Launcher", @ScriptDir & "\Discord Launcher.exe", "", "")
	MainMenu()
	Exit 0
EndIf

Func MainMenu()
	$Startup = IniRead(@ScriptDir & "\config.ini", "Startup", "Startup", "")

	If $Startup = "0" Then
		$DonationMSG = _Metro_MsgBox(0, "Discord Launcher 0.4", "If you find this program useful please use the donation button found during startup, or by using the link found in the README.txt file." & @CRLF & @CRLF & "To see the latest changes, please refer to the included patch notes.", 380, 11)
		;Checks to see if the Licence agreement has been agreed to and shows the licence agreement (VERY LONG LINE OF CODE, USE WRAP TO SEE!!)
		$LICENCE = _Metro_MsgBox(1, "Discord Launcher 0.4", "(C) TheChiefMeat 2016-2018. All rights reserved." & @CRLF & @CRLF & "DISCLAIMER/LICENCE" & @CRLF & @CRLF & "WHILE I HAVE MADE EVERY POSSIBLE EFFORT TO ENSURE THIS SOFTWARE IS BUG FREE," & @CRLF & "I DO NOT GUARANTEE THAT IT IS FREE FROM DEFECTS. THIS SCRIPT IS PROVIDED AS IS" & @CRLF & "AND YOU USE THIS SCRIPT AT YOUR OWN RISK. UNDER NO CIRCUMSTANCE SHALL I BE HELD" & @CRLF & "LIABLE FOR ANY DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES, LOSSES OR THEFT FROM USE OR MISUSE OF THIS SOFTWARE." & @CRLF & @CRLF & "FOR PRIVATE USE YOU MAY:" & @CRLF & @CRLF & "MODIFY THIS SOFTWARE ANY WAY YOU SEE FIT, HOWEVER SAID MODIFIED VERSIONS ARE NOT TO BE REDISTRIBUTED VIA ANY MEANS." & @CRLF & @CRLF & "FOR PRIVATE USE YOU MAY NOT:" & @CRLF & "REDISTRIBUTE THIS SOFTWARE." & @CRLF & "REDISTRIBUTE MODIFIED VERSIONS OF THIS SOFTWARE." & @CRLF & "COMMERCIALISE THIS SOFTWARE." & @CRLF & "SELL THIS SOFTWARE." & @CRLF & @CRLF & "ANY COMMERCIAL USE OF THIS SOFTWARE IS STRICTLY PROHIBITED." & @CRLF & "ANY COMMERCIALISATION OF THIS SOFTWARE IS STRICTLY PROHIBITED." & @CRLF & @CRLF & "ALL DOWNLOAD LINKS MUST POINT DIRECTLY TO THE BELOW LINK:" & @CRLF & @CRLF & "OFFICIAL BITBUCKET REPOSITORY:" & @CRLF & @CRLF & "https://bitbucket.org/TheChiefMeat/discord-launcher/downloads/discord-launcher-latest.zip", 650, 11)
		If $LICENCE = "Cancel" Then
			FileDelete("config.ini")
			Exit 0
		ElseIf $LICENCE = "OK" Then
			IniWrite(@ScriptDir & "\config.ini", "Startup", "Startup", "1")
		EndIf
	Else
	EndIf

	FileOpen(@ScriptDir & "\config.ini", 1)
	$Form1 = _Metro_CreateGUI("Discord Launcher 0.4", 198, 305, -1, -1, True)
	$DiscordButtonDefault = _Metro_CreateButton("Discord", 18, 20, 130, 40)
	$DiscordButton1Name = IniRead(@ScriptDir & "\config.ini", "Discord", "Account1Username", "")
	If $DiscordButton1Name = "" Then
		$DiscordButton1 = _Metro_CreateButton("Setup", 18, 65, 130, 40)
	Else
		$DiscordButton1 = _Metro_CreateButton($DiscordButton1Name, 18, 65, 130, 40)
	EndIf
	$ButtonBKColor = "0xFF3333"
	$Delete1 = _Metro_CreateButton("X", 153, 65, 40, 40)
	$ButtonBKColor = "0x512DA8"
	$DiscordButton2Name = IniRead(@ScriptDir & "\config.ini", "Discord", "Account2Username", "")
	If $DiscordButton2Name = "" Then
		$DiscordButton2 = _Metro_CreateButton("Setup", 18, 110, 130, 40)
	Else
		$DiscordButton2 = _Metro_CreateButton($DiscordButton2Name, 18, 110, 130, 40)
	EndIf
	$ButtonBKColor = "0xFF3333"
	$Delete2 = _Metro_CreateButton("X", 153, 110, 40, 40)
	$ButtonBKColor = "0x512DA8"
	$Guide = _Metro_CreateButton("Guide", 18, 155, 130, 40)
	$Exit = _Metro_CreateButton("Exit", 18, 200, 130, 40)
	$ButtonBKColor = "0x40C338"
	$Donate = _Metro_CreateButton("Donate", 18, 245, 130, 40)

	;Fixes the GUI positioning bug.
	GUICtrlSetResizing($DiscordButtonDefault, $GUI_DOCKALL + $GUI_DOCKBORDERS)
	GUICtrlSetResizing($DiscordButton1, $GUI_DOCKALL + $GUI_DOCKBORDERS)
	GUICtrlSetResizing($DiscordButton2, $GUI_DOCKALL + $GUI_DOCKBORDERS)
	GUICtrlSetResizing($Delete1, $GUI_DOCKALL + $GUI_DOCKBORDERS)
	GUICtrlSetResizing($Delete2, $GUI_DOCKALL + $GUI_DOCKBORDERS)
	GUICtrlSetResizing($Guide, $GUI_DOCKALL + $GUI_DOCKBORDERS)
	GUICtrlSetResizing($Exit, $GUI_DOCKALL + $GUI_DOCKBORDERS)
	GUICtrlSetResizing($Donate, $GUI_DOCKALL + $GUI_DOCKBORDERS)

	$Account1Username = IniRead(@ScriptDir & "\config.ini", "Discord", "Account1Username", "")
	$Account2Username = IniRead(@ScriptDir & "\config.ini", "Discord", "Account2Username", "")

	; Display the GUI.z
	GUISetState(@SW_SHOW)
	; Loop until the user exits. (This is required to keep the interface on the screen).
	While 1
		_Metro_HoverCheck_Loop($Form1)
		Switch GUIGetMsg()
			;Cases are "Events"; Things that happen on the screen. Here you can see the donation message occur, then the startup msg, etc.
			;Usually you want each "event" to be in their own Case.
			Case $GUI_EVENT_CLOSE
				ExitLoop
			Case $DiscordButtonDefault
				Run(@UserProfileDir & "\AppData\Local\Discord\update.exe -processStart discord.exe")
			Case $DiscordButton1
				If $Account1Username = "" Then
					$ButtonBKColor = "0x512DA8"
					$Setup = _Metro_MsgBox(0, "Discord Launcher 0.4", "FIRST TIME USE: Please provide a username and password (password doesn't matter)"  & @CRLF & @CRLF & "Program may appear unresponsive when creating a new instance.", 410, 11, "")

					$Form2 = _Metro_CreateGUI("Discord Launcher 0.4", 400, 160, -1, -1, True)
					$Username = GUICtrlCreateInput("", 60, 30, 280, 20)
					$sPasswd = GUICtrlCreateInput("", 60, 60, 280, 20, $ES_PASSWORD)
					$EXE_Button = _Metro_CreateButton("OK", 135, 100, 130, 40)
					GUISetState(@SW_SHOW)
					While 1
						_Metro_HoverCheck_Loop($Form2)
						Switch GUIGetMsg()
							Case $GUI_EVENT_CLOSE
							Case $EXE_Button
								$sPasswd = GUICtrlRead($sPasswd)
								$Username = GUICtrlRead($Username)
								IniWrite(@ScriptDir & "\config.ini", "Discord", "Account1Username", $Username)
								IniWrite(@ScriptDir & "\config.ini", "Discord", "Account1Password", $sPasswd)
								$Account1Username = IniRead(@ScriptDir & "\config.ini", "Discord", "Account1Username", "")
								Run("net user /add" & " " & $Username & " " & $sPasswd)
								DirCopy(@UserProfileDir & "\AppData\Local\Discord", "C:\Discord Launcher" & "\" & $Account1Username)
								_Metro_MsgBox(0, "Discord Launcher 0.4", "SETUP COMPLETE!", 400, 11)
								GUIDelete($Form2)
								ExitLoop
						EndSwitch
					WEnd
				Else
					$Account1Username = IniRead(@ScriptDir & "\config.ini", "Discord", "Account1Username", "")
					$Account1Password = IniRead(@ScriptDir & "\config.ini", "Discord", "Account1Password", "")
					$Run1 = ("C:\Discord Launcher\" & $Account1Username & "\run.bat")
					If FileExists($Run1) Then
						FileChangeDir("C:\Discord Launcher\" & $Account1Username)
						Run("C:\Discord Launcher\" & $Account1Username & "\run.bat")
					Else
						FileChangeDir("C:\Discord Launcher\" & $Account1Username)
						FileWriteLine("C:\Discord Launcher\" & $Account1Username & "\run.bat", "runas /savecred /user:" & $Account1Username & " " & '"update.exe -processStart discord.exe"')
						Run("C:\Discord Launcher\" & $Account1Username & "\run.bat")
					EndIf
				EndIf
			Case $Delete1
				$Account1Username = IniRead(@ScriptDir & "\config.ini", "Discord", "Account1Username", "")
				If $Account1Username = "" Then
				Else
					Run("net user" & " " & $Account1Username & " " & "/delete")
					DirRemove("C:\Discord Launcher\" & $Account1Username, 1)
					DirRemove("C:\Users\" & $Account1Username, 1)
					IniWrite(@ScriptDir & "\config.ini", "Discord", "Account1Username", "")
					IniWrite(@ScriptDir & "\config.ini", "Discord", "Account1Password", "")
					$ButtonBKColor = "0x512DA8"
					_Metro_MsgBox(0, "Discord Launcher 0.4", "        Account Deleted", 200, 11)
				EndIf
			Case $DiscordButton2
				If $Account2Username = "" Then
					$ButtonBKColor = "0x512DA8"
					$Setup = _Metro_MsgBox(0, "Discord Launcher 0.4", "FIRST TIME USE: Please provide a username and password (password doesn't matter)"  & @CRLF & @CRLF & "Program may appear unresponsive when creating a new instance.", 410, 11, "")

					$Form3 = _Metro_CreateGUI("Discord Launcher 0.4", 400, 150, -1, -1, True)
					$Username = GUICtrlCreateInput("", 60, 10, 280, 20)
					$sPasswd = GUICtrlCreateInput("", 60, 30, 280, 20, $ES_PASSWORD)
					$EXE_Button = _Metro_CreateButton("OK", 135, 80, 130, 40)
					GUISetState(@SW_SHOW)
					While 1
						_Metro_HoverCheck_Loop($Form3)
						Switch GUIGetMsg()
							Case $GUI_EVENT_CLOSE
							Case $EXE_Button
								$sPasswd = GUICtrlRead($sPasswd)
								$Username = GUICtrlRead($Username)
								IniWrite(@ScriptDir & "\config.ini", "Discord", "Account2Username", $Username)
								IniWrite(@ScriptDir & "\config.ini", "Discord", "Account2Password", $sPasswd)
								$Account2Username = IniRead(@ScriptDir & "\config.ini", "Discord", "Account2Username", "")
								Run("net user /add" & " " & $Username & " " & $sPasswd)
								DirCopy(@UserProfileDir & "\AppData\Local\Discord", "C:\Discord Launcher" & "\" & $Account2Username)
								_Metro_MsgBox(0, "Discord Launcher 0.4", "SETUP COMPLETE!", 400, 11)
								GUIDelete($Form3)
								ExitLoop
						EndSwitch
					WEnd
				Else
					$Account2Username = IniRead(@ScriptDir & "\config.ini", "Discord", "Account2Username", "")
					$Account2Password = IniRead(@ScriptDir & "\config.ini", "Discord", "Account2Password", "")
					$Run1 = ("C:\Discord Launcher\" & $Account2Username & "\run.bat")
					If FileExists($Run1) Then
						FileChangeDir("C:\Discord Launcher\" & $Account2Username)
						Run("C:\Discord Launcher\" & $Account2Username & "\run.bat")
					Else
						FileChangeDir("C:\Discord Launcher\" & $Account2Username)
						FileWriteLine("C:\Discord Launcher\" & $Account2Username & "\run.bat", "runas /savecred /user:" & $Account2Username & " " & '"update.exe -processStart discord.exe"')
						Run("C:\Discord Launcher\" & $Account2Username & "\run.bat")
					EndIf
				EndIf
			Case $Delete2
				$Account2Username = IniRead(@ScriptDir & "\config.ini", "Discord", "Account2Username", "")
				If $Account2Username = "" Then
				Else
					Run("net user" & " " & $Account2Username & " " & "/delete")
					DirRemove("C:\Discord Launcher\" & $Account2Username, 1)
					DirRemove("C:\Users\" & $Account2Username, 1)
					IniWrite(@ScriptDir & "\config.ini", "Discord", "Account2Username", "")
					IniWrite(@ScriptDir & "\config.ini", "Discord", "Account2Password", "")
					$ButtonBKColor = "0x512DA8"
					_Metro_MsgBox(0, "Discord Launcher 0.4", "        Account Deleted", 200, 11)
				EndIf
			Case $Guide
				$ButtonBKColor = "0x512DA8"
				$GuideMsg = _Metro_MsgBox(0, "Discord Launcher 0.4", 'The "Discord" button at the top will launch the default installation of Discord.' & @CRLF & @CRLF & 'Pressing either of the "Setup" buttons will prompt you to provide both a username and password (please note that the password does not matter, but is still required).' & @CRLF & @CRLF & 'Once your account has been created simply click the same "Setup" button you used before (note that the name will change to the username you provided after relaunching the program) to launch that instance of Discord.' & @CRLF & @CRLF & 'On first launch you will be required to enter the same password into the command prompt window that is shown, this will only be required once per instance.' & @CRLF & @CRLF & 'To delete an instance, simply close Discord down and press the red "X" button next to that instance.', 380, 11)
			Case $Donate
				ShellExecute("https://www.paypal.me/TheChiefMeat/5")
			Case $Exit
				GUIDelete($Form1)
				Exit 0
		EndSwitch
	WEnd
EndFunc
